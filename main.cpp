#include <iostream>
#include <regex>
#include <random>

#include "rxcpp/rx.hpp"

namespace Rx {
using namespace rxcpp;
using namespace rxcpp::sources;
using namespace rxcpp::operators;
using namespace rxcpp::util;
}
using namespace Rx;

using namespace std;
using namespace std::chrono;

int main() {

//  auto o1 = rxcpp::observable<>::interval(std::chrono::milliseconds(2));
//  auto o2 = rxcpp::observable<>::interval(std::chrono::milliseconds(3));
//  auto o3 = rxcpp::observable<>::interval(std::chrono::milliseconds(5));
//  auto values = o1.combine_latest([](int v1, int v2) {
//    return v1 + v2;
//  }, o2);
//  values.take(5).subscribe([](int v) {
//    printf("OnNext: %d\n", v);
//  });

  auto ints = rxcpp::observable<>::create<int>(
      [](rxcpp::subscriber<int> s) {
        for (int i = 0; i < 10; ++i) {
          s.on_next(i);
        }
        s.on_completed();
      });
  ints.subscribe(
      [](int v){printf("OnNext: %d\n", v);},
      [](){printf("OnCompleted\n");});

  return 0;
}
